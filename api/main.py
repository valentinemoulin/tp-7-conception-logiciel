import fastapi
import requests 
import uvicorn
import random as rand
import typing
from fastapi import FastAPI
from random import random 
from typing import List
from requests.exceptions import ConnectionError


app = FastAPI(
    title= "MUSIQUE ALÉATOIRE",
    description="Renvoie les informations d'une musique d'un artiste mis en entrée")

@app.get("/health")
async def health_check_api() -> bool:
    """Teste la bonne configuration et la disponibilité des services dont dépend l'API
    
    Args : 
    
    Returns : 
        bool : renvoie true si les deux API sont disponibles
    """
    response_audio = requests.get('https://www.theaudiodb.com/api/v1/json/2/search.php')
    response_lyrics = requests.get('https://lyrics.ovh')
    
    if response_audio.status_code == 200 and response_lyrics.status_code == 200:
        return(True)
    else:
        return(False)
    
@app.get("/")
def root():
    return()

@app.get("/random/{artist_name}")
async def random_artist_name(artist_name) -> dict:
    """Pour l'artiste mis en entrée vous recevrez les informations sur une de ses musiques 
    tirée aléatoirement

    Args:
        artist_name (str) : nom d'un l'artiste 

    Returns :
        dict: informations sur la musique 
    """
    
    if get_id_artiste(artist_name) != None : 
        
        id_albums = get_id_albums_by_id_artiste(get_id_artiste(artist_name))
        
        musics_artist = []
        
        for id_album in id_albums : 
            musics_artist += get_musics(id_album)
            
        alea = rand.randrange(0, len(musics_artist))
        musics_artist[alea]['lyrics'] = get_lyrics(musics_artist[alea]['title'], musics_artist[alea]['artist'])
        return musics_artist[alea]
    else :
        error = {"artist" : "Not found"}
        return(error)
        

def get_id_artiste(artist_name : str ) -> int:
    """ Renvoie l'identifiant de l'artiste mis en entrée
    
    Args:
        artist_name (str): nom de l artiste pour lequel on veut l'identifiant

    Returns:
        int : identifiant de l article
         
    """
    requete = requests.get("https://www.theaudiodb.com/api/v1/json/2/search.php?s={}".format(artist_name))
    
    if requete.status_code == 200 : 
        response = requete.json()
        try : 
            return response["artists"][0]['idArtist']
        except : 
            return response["artists"]
    else : 
        raise ConnectionError
        
def get_id_albums_by_id_artiste(id_artiste : int) -> list():
    """Récupère tous les identifiants des albums d'un article

    Arguments:
        id_artiste (int): identifiant de l'artiste pour lequel on veut la liste des identifiants d'albums

    Retourne:
        list(int): liste d'entiers correspond aux identifiants des albums 
    """
    requete = requests.get("https://theaudiodb.com/api/v1/json/2/album.php?i={}".format(id_artiste))
    id_albums = []
    
    if requete.status_code == 200 : 
        response = requete.json()
        if response["album"]==None:
            print("L'artiste ne possède pas d'albums ou l'artiste n'a pas été trouvé")
        else :
            for i in range(0,len(response["album"])):
                id_albums.append(response["album"][i]["idAlbum"])
        return id_albums
    else :
        raise ConnectionError


def get_musics(id_album : int) -> list(): 
    """Récupère les musiques d'un album dont l'identifiant a été mis en entrée

    Args:
        id_album (int): identifiant d'un album

    Returns:
        List(dict): liste des musiques de l'album 
    """
    
    musics = [] 
    requete = requests.get("https://theaudiodb.com/api/v1/json/2/track.php?m={}".format(id_album))
    
    if requete.status_code == 200 :
        response = requete.json()
        if response["track"] == None : 
            print("L'album", id_album,"n'a pas été trouvé")
            
        else :
            for i in range(0, len(response["track"])):
                music = {"artist" : response["track"][i]["strArtist"], 
                        "title" : response["track"][i]["strTrack"] , 
                        "suggested_youtube_url" : response["track"][i]["strMusicVid"]}
                musics.append(music)
        return musics
    else :
        raise ConnectionError


def get_lyrics(titre : str, artiste : str) -> str:
    """Récupère les paroles d'une musique

    Arguments:
        titre (str): Titre de la musique dont l'on veut les paroles
        artiste (str): Nom de l'artiste pour lequel on veut les paroles

    Retourne:
        str : Paroles de la musique si celles-ci ont été trouvé
    """
    
    try :
        requete = requests.get("https://api.lyrics.ovh/v1/{artiste}/{title}".format(artiste = artiste, title = titre))
        response = requete.json()
        try :
            return response["lyrics"]
        except : 
            return response["error"]
    except : 
        return("No lyrics found")


if __name__ == "__main__":
    uvicorn.run(app)


