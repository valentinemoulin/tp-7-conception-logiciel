# APPLICATION PERMETTANT D'OBTENIR UNE MUSIQUE ALÉATOIREMENT À PARTIR D'UN NOM D'ARTISTE

## SCHÉMA D'ARCHITECTURE
```mermaid
graph  TD;
  Client --> API ;
  API --> Client; 
  API-->AudioDB ;
  AudioDB-->API;
  API-->Lyricsovh;
  Lyricsovh-->API;
```

## RÉCUPÉRATION DU DÉPÔT 
```git clone https://gitlab.com/valentinemoulin/tp-7-conception-logiciel.git```

## LANCEMENT DE L'APPLICATION 
Placez vous dans le bon dossier : 
```cd tp-7-conception-logiciel```

Placez vous dans le dossier api : 
```cd api ```

Dans un premier temps, installer les dépendances qui sont dans le fichier "requirements.txt" avec la ligne de code suivante : 

```pip install -r requirements.txt```

Puis, lancer l'API :
```uvicorn main:app --reload```
ou 
```python3 main.py```

Ouvrer ensuite sur un navigateur votre lien url. Conserver ce lien url pour le mettre dans le fichier .env.


## LANCEMENT DU(/DES) TEST(S) 

```python test.py```

Le test doit être effectué lorsque l'API est en route. 
