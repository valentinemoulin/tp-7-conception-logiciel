import unittest
import requests
import os
from dotenv import load_dotenv
from main import *

# Récupération des données du .env
load_dotenv()

class Test(unittest.TestCase):

    def test_random_artist(self):
        """ Teste si l'API renvoie un dictionnaire avec valeur "Not Found" pour artist et non une erreur
        """
        request = requests.get(os.environ["URL"]+"/random/{}".format("tt"))
        response = request.json()
        self.assertEqual(response['artist'],"Not found")

if __name__ == '__main__':
    unittest.main()

    

    
