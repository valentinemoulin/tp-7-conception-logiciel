fastapi==0.75.2
python-dotenv==0.20.0
requests==2.23.0
uvicorn==0.17.6
