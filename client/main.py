from http.client import OK
import json
import requests 
import os
from dotenv import load_dotenv
import random as rand
import pandas as pd
import yaml

# Récupération des données du .env
load_dotenv()

with open(os.environ["FILE"]) as outfile : 
    musiques = json.load(outfile)

def get_playlist() : 
    """Création d'une playlist de longueur 20 (longueur pouvant être modifié dans le fichier configuration) 
    à partir d'un json téléchargé 
    
    Args : 
    
    Returns : 
        list : playlist
    """
    playlist = []
    
    while len(playlist) < int(os.environ["LENGTH_PLAYLIST"]) :
        # On prend une musique au hasard dans la liste
        alea_mus = rand.randrange(0,len(musiques)) 
        # On tire un nombre aleatoirement 
        alea = rand.randrange(0,20)
        
        if alea < musiques[alea_mus]['note'] : # On sélectionne l'artiste avec une proba proportionnelle à 
            # sa note
            request = requests.get(os.environ["URL"]+"/random/{}".format(musiques[alea_mus]['artiste']))
            response = request.json()
            playlist.append(response)
            
    return playlist

# Création de la playlist
playlist = get_playlist()

print(yaml.dump(playlist, sort_keys=False, default_flow_style=False))

# Mise en pandas data frame pour l'export
playlist = pd.DataFrame(playlist)

# Export de la playlist 
if os.environ["FORMAT"] =="json" :
    playlist.to_json("playlist.json")
elif os.environ["FORMAT"] == "csv":
    playlist.to_csv("playlist.csv")
elif os.environ["FORMAT"] == "excel": 
    playlist.to_excel("playlist.xlsx")