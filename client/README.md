### README DU CÔTÉ CLIENT : CRÉATION D'UNE PLAYLIST DE 20 MUSIQUES

Pour cela, vous aurez besoin d'un fichier répertoriant des artistes et une note attribuée à chacun. 
Placez ce fichier dans le dossier client. Ce dossier doit être au format **json**. 
Puis changer le nom dans le fichier ".env" qui se trouver à la racine du projet selon le nom de votre json. \n

Dans le même fichier .env, saisissez la longueur de la playlist que vous souhaitez et sous quel format vous la souhaitez. 

Installer les dépendances nécessaires pour la création de votre playlist : 
```pip install -r requirements.txt```

L'API doit être en train de tourner pour que vous puissiez obtenir une playlist. Pour cela, ouvrez un deuxième terminal et basez-vous sur le README côté API.

Une fois que l'API est en route, remettez vous sur votre terminal et lancez le code suivant :  
```python3 main.py```

Vous obtiendrez une playlist prenant en compte les notes que vous avez mis pour chaque artiste. En effet, chaque artiste aura une probabilité proportionnelle à sa note.  
Une fois que le code aura tourné, et que votre playlist est prête celle-ci sera disponible au format csv, json ou excel selon ce que vous aurez indiqué dans le fichier .env.
